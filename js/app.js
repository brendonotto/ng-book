var app = angular.module('app', []);
app.controller('MyController', function($scope, $timeout) {
        var updateClock = function() {
            $scope.clock = {};
            $scope.clock.now = new Date();
            $timeout(function() {
                updateClock();
            }, 1000);
        };
        updateClock();
    });
app.controller('FirstController', function($scope) {
    $scope.counter = 0;
    $scope.add = function(amount) { $scope.counter += amount; };
    $scope.subtract = function(amount) { $scope.counter -= amount; };
});
app.controller('ParentController', function($scope) {
    $scope.person = {greeted: true};
});
app.controller('ChildController', function($scope) {
    $scope.sayHello = function() {
        $scope.person.name = "Ari Lerner";
    }
});